﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.IO;
using System.Data.OleDb;
using Excel = Microsoft.Office.Interop.Excel;




namespace Amazon_Filler
{
    public partial class Form1 : Form
    {
       
        enum CellData
        {
         

       FileName=1,
        Gender=2,
        Color1=3,
        Color2 = 4,
        Color3 = 5,
        Color4 = 6,
        Color5 = 7,
        Price=8,
        Brand=9,
        Title=10,
        B1=11,
        B2=12,
        Des=13,
        Sub=14,
                }


        List<string> XLData = new List<string>();
        string path1 = "";

        static string path = Application.StartupPath.ToString().Replace("\\bin\\Debug", "");
        string pathxl = path + "\\DB\\Amazon Input.xlsx";


        public Form1()
        {
            InitializeComponent();



       
            path1 = path + "\\DB\\add_details.htm";
      
            webBrowser1.Navigate(path1);

            label1.Text = path;

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            int i = 0;


            i = dataGridView1.CurrentRow.Index;
         //   if (i > 1) i = i + 5;


            //get page element with id 

            string link = webBrowser1.Url.ToString().ToLower();
            if (link.Contains("add_details"))
            {
                //Add Prouct Details Page
                // Load data to fields.

                //Brand Name
                webBrowser1.Document.GetElementById("data-draft-brand-name").InnerText = dataGridView1.Rows[i].Cells[(int)CellData.Brand].Value.ToString();

                // Title of product 
                webBrowser1.Document.GetElementById("data-draft-name-en-us").InnerText = dataGridView1.Rows[i].Cells[(int)CellData.Title].Value.ToString();



                // List Price
                webBrowser1.Document.GetElementById("data-draft-list-prices-marketplace-1-amount").InnerText = dataGridView1.Rows[i].Cells[(int)CellData.Price].Value.ToString();

                //Key product feature 1
                webBrowser1.Document.GetElementById("data-draft-bullet-points-bullet1-en-us").InnerText = dataGridView1.Rows[i].Cells[(int)CellData.B1].Value.ToString();

                // Key Prouct feature 2
                webBrowser1.Document.GetElementById("data-draft-bullet-points-bullet2-en-us").InnerText = dataGridView1.Rows[i].Cells[(int)CellData.B2].Value.ToString();

                // Product description
                webBrowser1.Document.GetElementById("data-draft-description-en-us").InnerText = dataGridView1.Rows[i].Cells[(int)CellData.Des].Value.ToString();
            }
            //  case
            else if (link.Contains("variations"))
            {
                //checkbox selection

                if (XLData[i + 1]=="Male") {
                webBrowser1.Document.GetElementById("data-shirt-configurations-fit-type-men").InvokeMember("CLICK");
                }

            }

            else
            {

            }

            

        }




        private void button4_Click(object sender, EventArgs e)
        {
            // Load Excel file and add to data grid. 

            dataGridView1.DataSource = null;
            dataGridView1.Columns.Clear();
            this.dataGridView1.Rows.Clear();
           






            //openFileDialog1.ShowDialog();
            if (openFileDialog1.ShowDialog(this) == DialogResult.OK)
            {
                pathxl = openFileDialog1.InitialDirectory + openFileDialog1.FileName;
            }

            //Load Excel

            Excel.Application xlApp = new Excel.Application();
            Excel.Workbook xlWorkbook = xlApp.Workbooks.Open(pathxl);
            Excel._Worksheet xlWorksheet = xlWorkbook.Sheets[1];
            Excel.Range xlRange = xlWorksheet.UsedRange;


           

            for (int i = 1; i <= xlRange.Rows.Count; i++)
            {

            


                for (int j = 1; j <= 15; j++)
                {
                    if (xlRange.Cells[i, j] != null && xlRange.Cells[i, j].Value2 != null)

                    {
                        if (i == 1)
                        {

                            dataGridView1.Columns.Add("", xlRange.Cells[i, j].Value2.ToString());
                            continue;

                        }


                      //  xlRange.Cells[i, j].value
                        // XLData.Add(xlRange.Cells[i, j].Value2.ToString());
                        var valuetest= xlRange.Cells[i, j].Value.ToString();

                        dataGridView1.Rows[i-2].Cells[j-1].Value = xlRange.Cells[i, j].Value.ToString();
                    }
                }
               if (xlRange.Cells[i + 1, 1].Value2 != null)

                    dataGridView1.Rows.Add();

            }

         //   dataGridView1.Sort(dataGridView1.Columns[0], ListSortDirection.Ascending);




            //dataGridView1.Rows.Add(XLData[n - 2], XLData[n - 1], XLData[n], XLData[n + 1], XLData[n + 2], XLData[n + 3]);
            //n += 6;


            xlWorkbook.Close();
            xlApp.Quit();





        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            string filePath = openFileDialog1.FileName;
            string extension = Path.GetExtension(filePath);


     

            //switch (extension)
            //    {
            //    case  ".xls" :
            //    //Excel 97-03
            //    conStr = String.Format(Excel03ConString, filePath, header);
            //   break;

            //case  ".xlsx" :
            //        //Excel 07 and above
            //        conStr = String.Format(Excel07ConString, filePath, header);
            //      break;


            //    default :
            //        MessageBox.Show("This is not an excel file");
            //    break;
            }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Next_Click(object sender, EventArgs e)
        {
            path1 = path + "\\DB\\choose_variations.htm";

            webBrowser1.Navigate(path1);
        }

    }

   
    }
